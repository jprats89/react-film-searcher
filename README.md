#### Description
This project was created following a [React course](https://www.udemy.com/course/aprendiendo-react/) in order to learn basic concepts. 
<b>React Router</b> and <b>Prop Types</b> were used to improve the example app created

#### Run the project

To check the project just run from the root:

##### `npm start`
