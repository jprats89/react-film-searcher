import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {BackButton} from "../components/BackButton";

const api_key = '1877c82';

export class Detail extends Component {
  static propTypes = {
    match: PropTypes.shape({
      params: PropTypes.shape({
        id: PropTypes.string
      })
    })
  }
  state = { movie: {} };

  componentDidMount() {
    fetch(`http://www.omdbapi.com/?apikey=${api_key}&i=${this.props.match.params.id}`)
      .then(result => result.json())
      .then(result => {
        console.log(result);
        this.setState({movie: result})
      });
  }

  render() {
    const { Title, Poster, Actors, Metascore, Plot } = this.state.movie;
    return (
      <div>
        <div style={{margin: 20}}>
          <BackButton/>
        </div>
        <h1>{Title}</h1>
        <img src={Poster} alt="Film Poster"/>
        <h3>{Actors}</h3>
        <span>{Metascore}</span>
        <p>{Plot}</p>
      </div>
    );
  }
}