import React from 'react';
import {BackButton} from '../components/BackButton';

export const NotFound = () => (
    <div style={{margin: 20}}>
      <h1 className="title">Page not found</h1>
        <BackButton/>
    </div>
)