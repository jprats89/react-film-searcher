import React, {Component} from 'react';
import {Title} from '../components/Title';
import {SearchForm} from '../components/SearchForm';
import {MoviesList} from '../components/MoviesList';

export class Home extends Component {
  state = {
    results: [],
    searchDone: false
  };

  _handleResults = (results) => {
    this.setState({results});
    this.setState({searchDone: true});
  };

  _renderResults = () => {
    return (this.state.results.length === 0
        ? 'No results'
        : <MoviesList movies={this.state.results}/>
    )
  }

  render() {
    return (
      <div className="App">
        <Title>Movies searcher</Title>
        <div className="has-text-centered">
          <SearchForm onResults={this._handleResults}/>
          {
            this.state.searchDone
              ? this._renderResults()
              : 'Type a title to see the result'
          }
        </div>
      </div>

    )
  }
}