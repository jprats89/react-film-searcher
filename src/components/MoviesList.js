import React, {Component} from 'react';
import {Movie} from './Movie';

export class MoviesList extends Component {
  render() {
    return (
      this.props.movies.map(movie => {
        return <Movie key={movie.imdbID} id={movie.imdbID} title={movie.Title} poster={movie.Poster} year={movie.Year}/>
      })
    )
  }
}
