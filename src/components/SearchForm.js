import React, {Component} from 'react';
const api_key = '1877c82';

export class SearchForm extends Component {
  state = {
    inputMovie: ''
  };

  _handleChange = (e) => {
    this.setState({ inputMovie: e.target.value });
  };

  _handleSubmit = (e) => {
    e.preventDefault();
    fetch(`http://www.omdbapi.com/?apikey=${api_key}&s=${this.state.inputMovie}`)
      .then(results => results.json())
      .then(results => {
        console.log(results);
        const {Search = []} = results;
        this.props.onResults(Search);
      });
  }

  render(){
    return (
      <form onSubmit={this._handleSubmit}>
        <div className="field has-addons has-addons-centered">
          <div className="control">
            <input onChange={this._handleChange} className="input" type="text" placeholder="Find a repository"/>
          </div>
          <div className="control">
            <button className="button is-info"> Search </button>
          </div>
        </div>
      </form>
    )
  }
}