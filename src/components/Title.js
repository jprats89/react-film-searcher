import React from 'react';

export const Title = ({children}) => (
  <h1 className="title mt-4">{children}</h1>
)
