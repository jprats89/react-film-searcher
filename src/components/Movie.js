import React, {Component} from 'react';
import {Link} from 'react-router-dom';

export class Movie extends Component {
  render() {
    return (
      <Link to={`/detail/${this.props.id}`} className="card">
        <div className="card-image">
          <figure className="image is-4by3">
            <img src={this.props.poster} alt="Poster"/>
          </figure>
        </div>
        <div className="card-content">
          <div className="media">
            <div className="media-content">
              <p className="title is-4">{this.props.title}</p>
              <p className="subtitle is-6">{this.props.year}</p>
            </div>
          </div>
        </div>
      </Link>
    );
  }
}